#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#pragma pack(push, 1)
struct bmp_header {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};
#pragma pack(pop)

struct pixel { uint8_t b, g, r; };

struct image {
    uint64_t width, height;
    struct pixel* data;
};

enum read_status {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER,
    READ_ERROR
};

enum write_status {
    WRITE_OK = 0,
    WRITE_ERROR
};

enum read_status from_bmp(FILE* in, struct image* img) {
    struct bmp_header header;
    // Read and validate the BMP file header
    if (fread(&header, sizeof(header), 1, in) != 1) {
        return READ_ERROR;
    }
    if (header.bfType != 0x4D42) { // 'BM'
        return READ_INVALID_SIGNATURE;
    }
    if (header.biBitCount != 24) {
        return READ_INVALID_BITS;
    }

    img->width = header.biWidth;
    img->height = header.biHeight;
    img->data = malloc(img->width * img->height * sizeof(struct pixel));
    if (img->data == NULL) {
        return READ_ERROR;
    }

    size_t padding = (4 - (img->width * sizeof(struct pixel)) % 4) % 4;
    uint8_t pad[3];

    for (uint32_t i = 0; i < img->height; ++i) {
        if (fread(img->data + i * img->width, sizeof(struct pixel), img->width, in) != img->width) {
            free(img->data);
            return READ_ERROR;
        }
        if (fread(pad, 1, padding, in) != padding) {
            free(img->data);
            return READ_ERROR;
        }
    }

    return READ_OK;
}

struct image rotate(struct image const source) {
    struct image rotated;
    rotated.width = source.height;
    rotated.height = source.width;
    rotated.data = malloc(rotated.width * rotated.height * sizeof(struct pixel));
    if (!rotated.data) {
        return rotated;
    }

    for (uint64_t y = 0; y < source.height; y++) {
        for (uint64_t x = 0; x < source.width; x++) {
            rotated.data[(rotated.width - y - 1) + x * rotated.width] = source.data[x + y * source.width];
        }
    }

    return rotated;
}

enum write_status to_bmp(FILE* out, struct image const* img) {
    struct bmp_header header;
    header.bfType = 0x4D42; // 'BM'
    header.biSize = 40;
    header.bfileSize = 54 + img->width * img->height * sizeof(struct pixel);
    header.bfReserved = 0;
    header.bOffBits = 54;
    header.biWidth = img->width;
    header.biHeight = img->height;
    header.biPlanes = 1;
    header.biBitCount = 24;
    header.biCompression = 0;
    header.biSizeImage = img->width * img->height * sizeof(struct pixel);
    header.biXPelsPerMeter = 0;
    header.biYPelsPerMeter = 0;
    header.biClrUsed = 0;
    header.biClrImportant = 0;

    if (fwrite(&header, sizeof(header), 1, out) != 1) {
        return WRITE_ERROR;
    }

    size_t padding = (4 - (img->width * sizeof(struct pixel)) % 4) % 4;
    uint8_t pad[3] = {0};

    for (uint64_t y = 0; y < img->height; y++) {
        if (fwrite(img->data + y * img->width, sizeof(struct pixel), img->width, out) != img->width) {
            return WRITE_ERROR;
        }
        if (padding > 0) {
            if (fwrite(pad, 1, padding, out) != padding) {
                return WRITE_ERROR;
            }
        }
    }

    return WRITE_OK;
}

int main(int argc, char* argv[]) {
    if (argc != 3) {
        fprintf(stderr, "Usage: %s <source-image> <transformed-image>\n", argv[0]);
        return EXIT_FAILURE;
    }

    FILE* in = fopen(argv[1], "rb");
    if (!in) {
        perror("Error opening input file");
        return EXIT_FAILURE;
    }

    struct image img;
    if (from_bmp(in, &img) != READ_OK) {
        fclose(in);
        fprintf(stderr, "Error reading BMP file\n");
        return EXIT_FAILURE;
    }
    fclose(in);

    struct image rotated_img = rotate(img);
    free(img.data);

    FILE* out = fopen(argv[2], "wb");
    if (!out) {
        free(rotated_img.data);
        perror("Error opening output file");
        return EXIT_FAILURE;
    }

    if (to_bmp(out, &rotated_img) != WRITE_OK) {
        fclose(out);
        free(rotated_img.data);
        fprintf(stderr, "Error writing BMP file\n");
        return EXIT_FAILURE;
    }

    fclose(out);
    free(rotated_img.data);
    return EXIT_SUCCESS;
}
